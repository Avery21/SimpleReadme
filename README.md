# SimpleReadme

> You can add a README to your GitHub profile to tell other people about yourself. ([docs](https://docs.github.com/en/github/setting-up-and-managing-your-github-profile/customizing-your-profile/managing-your-profile-readme))

This repo helps you make a good looking readme. You can just copy and paste or fill out [this](http://www.averychan.site/SimpleReadme/) (hosted with github pages) to generate the README text for you.

<details>

  <summary>Example Output:</summary>
  
---

# Hi, I'm FIRST_LAST 👋

**(BIO GOES HERE)** Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.

<div align="center">
  <a href="LINKED_IN_PROFILE_LINK"><img alt="LinkedIn" src="https://img.shields.io/badge/FIRST_LAST-%230077B5.svg?style=flat&logo=linkedin&logoColor=white"/></a>
  <a href="mailto:MY_EMAIL@gmail.com"><img alt="Email" src="https://img.shields.io/badge/MY_EMAIL@gmail.com-D14836?style=flat&logo=gmail&logoColor=white"/></a>
  <a href="http://MYWEBSITE.com/"><img alt="Website" src="https://img.shields.io/website?down_color=lightgrey&down_message=offline&label=MYWEBSITE.com&up_color=green&up_message=online&url=http://MYWEBSITE.com/"/></a>
  <a href="./PATH_TO_RESUME.pdf"><img alt="Resume" src="https://img.shields.io/badge/Resume_(last_updated)-TIME_LAST_UPDATED-green"/></a>
</div><br/>

---
</details>

<details>

  <summary>Example Source:</summary>
  
```
# Hi, I'm FIRST_LAST 👋

**(BIO GOES HERE)** Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.

<div align="center">
  <a href="LINKED_IN_PROFILE_LINK"><img alt="LinkedIn" src="https://img.shields.io/badge/FIRST_LAST-%230077B5.svg?style=flat&logo=linkedin&logoColor=white"/></a>
  <a href="mailto:MY_EMAIL@gmail.com"><img alt="Email" src="https://img.shields.io/badge/MY_EMAIL@gmail.com-D14836?style=flat&logo=gmail&logoColor=white"/></a>
  <a href="http://MYWEBSITE.com/"><img alt="Website" src="https://img.shields.io/website?down_color=lightgrey&down_message=offline&label=MYWEBSITE.com&up_color=green&up_message=online&url=http://MYWEBSITE.com/"/></a>
  <a href="./PATH_TO_RESUME.pdf"><img alt="Resume" src="https://img.shields.io/badge/Resume_(last_updated)-TIME_LAST_UPDATED-green"/></a>
</div><br/>
```
</details>

[My profile in this style](https://github.com/Avery2)

## Instructions (Automatic)

Use the autogenerator here: http://www.averychan.site/SimpleReadme/

(if the link is ever down, just download this repo and use index.html)

## Instructions (Manual)

1. Create a repository that is the same as your github username _(for example: GitHubUser/GitHubUser)_
2. Create a README.md in that repository
3. Add the above text, and make replacements as described below
4. Add your Resume to the respository _(optional)_
5. Replace the bio placeholder
6. Done! It should show up at your profile.

## Replacement

Replace the following text. Count included to make sure you get each one.

| Text to replace        | Count | Description                                                |
|------------------------|-------|------------------------------------------------------------|
| FIRST_LAST             | 3     | Your name                                                  |
| LINKED_IN_PROFILE_LINK | 1     | Link to your LinkedIn profile                              |
| MY_EMAIL               | 2     | Your email (assumes Gmail extension)                       |
| MYWEBSITE              | 3     | Your website                                               |
| PATH_TO_RESUME         | 1     | The path to your resume file (you can add it to this repo) |
| TIME_LAST_UPDATED      | 1     | Last time your Resume was updated. (Ex: "Jun 2001")        |


## 🛠 Tools for this README

| Tool | Link |
|---|---:|
| Sheilds.io for the badges | [link](https://shields.io) |

## Other Resources

| Tool | Link |
|---|---:|
| Profile-readme for recent activity | [link](https://github.com/actions-js/profile-readme) |
| Github-readme-stats for the GitHub stats summary | [link](https://github.com/anuraghazra/github-readme-stats) |
| Productive-box for pinned gist (below) of commit times | [link](https://github.com/maxam2017/productive-box) ||
| List of many tools and examples | [link](https://github.com/abhisheknaiidu/awesome-github-profile-readme) |

